# tp-bootstrap-react #

This is a skeleton repository for bootstrapping browser-based projects with React.

## Quick start

Clone this repository:

    $ git clone ...

Rename the directory to whatever the name of your project is:

    $ mv react my-best-project

Remove the remote origin branch:

    $ cd my-best-project
    $ git remote remove origin

Now, you can set up your own, remote repository.

At first, you should set up the Node.js environment:

    $ npm install

Now, you should be able to use the following npm scripts:

    $ npm run unit-tests
    $ npm run e2e-tests
    $ npm run build
    $ npm run dist

The `unit-tests` script runs the unit tests, using mocha, [should](http://shouldjs.github.io/) and [sinon](http://sinonjs.org/).

The `e2e-tests` script runs the e2e tests, using [Nightwatch](http://nightwatchjs.org/). Please refer to the Nightwatch project, in order to properly configure the environment for those tools.

The `build` script builds the development version of your application.

The `dist` script builds the production version of your application.

## Contribution guidelines

If you have any questions, don't hesitate to e-mail me: tprimke(at)gmail(dot)com.

